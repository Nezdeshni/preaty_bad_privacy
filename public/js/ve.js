const CTEXT="полнодюплексный протокол с аппаратным квитированием пакетов"
const VE_KEY="антропоморфныйдендромутант"
var cText="";
var oText="";

function shuffle(arr){
	var al=arr.length;
	let npairs=al*al;
	while(npairs--){
		let a=~~(Math.random()*al);
		let b=~~(Math.random()*al);
		let t=arr[a];
		arr[a]=arr[b];
		arr[b]=t;
	}
	return arr
}
Array.prototype.shuffle=function(){return shuffle(this)}
Array.prototype.rotation=function(n){

return this.map((e,i,a)=>{ return a[(a.length+(n+i))%a.length]})
}; 



var VGEncoder={
	alphabeth:"абвгдеёжзийклмнопрстуфхцчшщъыьэюя".split(''),
	table:{},
	shiftLength:4
	}


VGEncoder.alphabets=VGEncoder.alphabeth.map((e,i,a)=>{
	let n=a.rotation(i);
	VGEncoder.table[n[0]]=[n[0],...n.slice(1).shuffle()]
	return VGEncoder.table[n[0]]})


var CaesarEncoder={
	alphabeth:"абвгдеёжзийклмнопрстуфхцчшщъыьэюя".split(''),
	shiftLength:4
	}
function cencode(strIn){
  var _self=this;
  return strIn.split('')
			.map(
				sym=>{
					let np=(_self.alphabeth.indexOf(sym.toLowerCase())
                        +_self.shiftLength)%_self.alphabeth.length;
				   return _self.alphabeth[np];
				}).join('')
	}

function cdecode(strIn){
  var _self=this;
  return strIn.split('')
			.map(
				sym=>{
					let np=(_self.alphabeth.indexOf(sym.toLowerCase())+_self.alphabeth.length-_self.shiftLength)%_self.alphabeth.length;
				   return _self.alphabeth[np];
				}).join('')
	}

CaesarEncoder.encode=cencode.bind(CaesarEncoder);
CaesarEncoder.decode=cdecode.bind(CaesarEncoder);

cText=CaesarEncoder.encode(CTEXT);
oText=CaesarEncoder.decode(cText);
console.log(
`   Caesar test case:
                text2crypt=${CTEXT};    key=${CaesarEncoder.shiftLength};
                ________________________________________________________
                Encrypt([${CTEXT}],[${CaesarEncoder.shiftLength}]) -> [${cText}]
                Decrypt([${cText}]+[${CaesarEncoder.shiftLength}]) -> [${oText}]
                _________________________________________________________
    `
);

/******************************************************************************************/
